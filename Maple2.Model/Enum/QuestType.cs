﻿namespace Maple2.Model.Enum;

public enum QuestType {
    EpicQuest = 0,
    WorldQuest = 1,
    EventQuest = 2,
    DailyMission = 3,
    FieldMission = 4,
    EventMission = 5,
    GuildQuest = 6,
    MentoringMission = 7,
    FieldQuest = 8,
    AllianceQuest = 9,
    WeddingMission = 10,
}
